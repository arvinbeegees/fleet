//
//  Constants.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 09/09/2022.
//

import Foundation

let BASE_URL = "https://app.ecofleet.com/seeme/Api"
let API_KEY = "PrNmpPZd65heVNIN6QYB3VbC1Yjl9pq4"

enum Endpoint: String {
    case vehicles = "/Vehicles/getLastData"
    case tripHistory = "/Vehicles/getRawData"
}

enum HTTPMethod: String {
    case get     = "GET"
    case post    = "POST"
}

