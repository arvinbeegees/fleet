//
//  LocationHistoryCreator.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 10/09/2022.
//

import UIKit

class LocationHistoryCreator: BaseCreator {
    
    override class func getStoryboardName() -> String {
        return "LocationHistory"
    }
    
    override class func getNavigationControllerIdentifier() -> String {
        return "LocationHistoryNC"
    }
    
    static func pushScene(_ vm: LocationHistoryVCVM) -> LocationHistoryVC {
        return setData(vm).topViewController as! LocationHistoryVC
    }
    
    static func presentScene(_ vm: LocationHistoryVCVM) -> UINavigationController {
        return setData(vm)
    }
    
    static private func setData(_ vm: LocationHistoryVCVM) -> UINavigationController {
        let nc = initNavigationController()
        
        if let vc = nc.topViewController as? LocationHistoryVC {
            vc.presenter = LocationHistoryPresenter.init(
                vm: vm,
                tripHistoryDataManager: TripHistoryDataManager()
            )
        }
        
        return nc
    }
}
