//
//  LocationHistoryVC.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 10/09/2022.
//

import UIKit
import GoogleMaps
import DatePicker
import ProgressHUD

@objc protocol LocationHistoryVCDelegate {
    func onSuccessRetrieveTripHistory()
    func onError(_ errorMessage: String)
}

class LocationHistoryVC: BaseVC {
    
    @IBOutlet weak var mTextField: UITextField!
    @IBOutlet weak var mMapView: GMSMapView!
    
    var label: UILabel!
    var polygon: GMSPolygon!
    var presenter: LocationHistoryPresenter!
    
    let group = DispatchGroup()
    
    override func viewDidLoad() {
        presenter.setViewDelegate(self)
        initUI()
        initData()
    }
    
    func initUI() {
        mTextField.isUserInteractionEnabled = false
        mTextField.text = Date().shortDate()
    }
    
    func initData() {
        ProgressHUD.show()
        presenter.setInitialTime()
        presenter.getLocationHistoryList()
    }
    
    func addTripDistanceLabel(distance: String) {
        if label != nil {
            label.removeFromSuperview()
        }
        
        label = UILabel()
        label.frame = CGRect(
            x: (UIScreen.main.bounds.width / 4),
            y: UIScreen.main.bounds.height - 60,
            width: 300,
            height: 40
        )
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.black
        label.text = "Trip Distance: \(distance) km"
        view.addSubview(label)
    }
    
    func initMapView() {
        let path = GMSMutablePath()
        group.enter()
        for i in presenter.vm.coordinates {
            path.add(
                CLLocationCoordinate2D(
                    latitude: i.latitude,
                    longitude: i.longitude
                )
            )
        }
        group.leave()
        
        group.notify(queue: DispatchQueue.global()) {
            DispatchQueue.main.async {
                // Create the polygon, and assign it to the map.
                
                if self.polygon != nil {
                    self.polygon.map = nil
                }
                
                self.polygon = GMSPolygon(path: path)
                self.polygon.fillColor = UIColor(
                    red: 0.25, green: 0, blue: 0, alpha: 0.05
                );
                self.polygon.strokeColor = .blue
                self.polygon.strokeWidth = 5
                self.polygon.map = self.mMapView
                
                if path.count() != 0 {
                    var bounds = GMSCoordinateBounds()
                    for index in 1...path.count() {
                        bounds = bounds.includingCoordinate(path.coordinate(at: index))
                        
                    }
                    self.mMapView.animate(with: GMSCameraUpdate.fit(bounds))
                } else {
                    self.showAlert(title: "No location history found.")
                }
            }
         }
    }
    
    func showAlert(title: String) {
        let alertController = UIAlertController(
            title: title,
            message: nil,
            preferredStyle: .alert
        )
        let okAction = UIAlertAction(
            title: "Ok",
            style: .cancel
        )
        alertController.addAction(okAction)
        present(alertController, animated: true)
    }
    
    @IBAction func onTapCalendar(_ sender: UIButton) {
        let minDate = DatePickerHelper.shared.dateFrom(
            day: 18, month: 08, year: 1990
        )!
        let maxDate = DatePickerHelper.shared.dateFrom(
            day: 18, month: 08, year: 2030
        )!
        let today = Date()
        let datePicker = DatePicker()
        datePicker.setup(beginWith: today, min: minDate, max: maxDate) {
            (selected, date) in
            if let date = date {
                self.presenter.vm.begTimestamp = date.longDate()
                self.mTextField.text = date.shortDate()
                ProgressHUD.show()
                self.presenter.getLocationHistoryList()
            }
        }
        datePicker.show(in: self, on: sender)
    }
}

extension LocationHistoryVC: LocationHistoryVCDelegate {
    
    func onSuccessRetrieveTripHistory() {
        ProgressHUD.dismiss()
        initMapView()
        addTripDistanceLabel(distance: "\(presenter.totalDistance)")
    }
    
    func onError(_ errorMessage: String) {
        ProgressHUD.dismiss()
        showAlert(title: errorMessage)
    }
}
