//
//  LocationHistoryVCVM.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 10/09/2022.
//

import Foundation

struct LocationHistoryVCVM {
    
    var coordinates: [CoordinateVM] = []
    let objectId: Int
    let apiKey: String
    var begTimestamp: String = ""
    var endTimestamp: String = ""
}
