//
//  LocationHistoryPresenter.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 10/09/2022.
//

import Foundation

class LocationHistoryPresenter {
    
    var vm: LocationHistoryVCVM!
    var tripHistoryDataManager: TripHistoryDataManager!
    
    init(vm: LocationHistoryVCVM,
         tripHistoryDataManager: TripHistoryDataManager) {
        self.vm = vm
        self.tripHistoryDataManager = tripHistoryDataManager
        
    }
    
    weak private var mVCDelegate : LocationHistoryVCDelegate?
    
    var totalDistance: Double {
        let distance = vm.coordinates
            .map { $0.distance }
            .reduce(0, +)
        return distance.roundToDecimal(3)
    }
    
    func setViewDelegate(_ vcDelegate: LocationHistoryVCDelegate?) {
        self.mVCDelegate = vcDelegate
    }
    
    func getLocationHistoryList() {
        let parameter = TripHistoryParameter(
            begTimestamp: vm.begTimestamp,
            endTimestamp: vm.endTimestamp,
            objectId: vm.objectId,
            key: vm.apiKey
        )
        
        tripHistoryDataManager.request(parameter: parameter) { [weak self] response in
            if let response = response {
                self?.vm.coordinates = self?.transform(response) ?? []
                self?.mVCDelegate?.onSuccessRetrieveTripHistory()
            }
        } failure: { [weak self] error in
            self?.mVCDelegate?.onError(error.errormessage)
        }
    }
    
    func setInitialTime() {
        vm.begTimestamp = Date.yesterday.longDate()
        vm.endTimestamp = Date().longDate()
    }
    
    fileprivate func transform(_ response: TripLocationListResponse) -> [CoordinateVM] {
        return response.response.map {
            return CoordinateVM(
                timestamp: $0.timestamp,
                latitude: $0.Latitude ?? 0,
                longitude: $0.Longitude ?? 0,
                distance: $0.Distance ?? 0
            )
        }
    }
}
