//
//  VehiclesVCVM.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 09/09/2022.
//

struct VehiclesVCVM {
    var listing: [VehiclesTVCellVM] = []
    var apiKey: String = ""
}
