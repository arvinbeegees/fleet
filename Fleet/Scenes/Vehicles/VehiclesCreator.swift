//
//  VehiclesCreator.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 09/09/2022.
//

import UIKit

class VehiclesCreator: BaseCreator {
    
    override class func getStoryboardName() -> String {
        return "Vehicles"
    }
    
    override class func getNavigationControllerIdentifier() -> String {
        return "VehiclesNC"
    }
    
    static func pushScene(_ vm: VehiclesVCVM) -> VehiclesVC {
        return setData(vm).topViewController as! VehiclesVC
    }
    
    static func presentScene(_ vm: VehiclesVCVM) -> UINavigationController {
        return setData(vm)
    }
    
    static private func setData(_ vm: VehiclesVCVM) -> UINavigationController {
        let nc = initNavigationController()
        
        if let vc = nc.topViewController as? VehiclesVC {
            vc.router = VehiclesRouter.init(vc)
            vc.presenter = VehiclesPresenter(
                vm: vm,
                vehiclesDataManager: VehiclesDataManager()
            )
        }
        
        return nc
    }
}
