//
//  VehiclesRouter.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 10/09/2022.
//

import UIKit

class VehiclesRouter: BaseRouter {
    
    // MARK: Properties - Private
    private weak var mVC: VehiclesVC!
    
    init(_ vc: VehiclesVC) {
        mVC = vc
    }
    
    // MARK: Location History
    func navigateToLocationHistory(objectId: Int, apiKey: String) {
        push(
            to: LocationHistoryCreator.pushScene(
                LocationHistoryVCVM(
                    objectId: objectId,
                    apiKey: apiKey
                )
            ),
            via: mVC
        )
    }
}
