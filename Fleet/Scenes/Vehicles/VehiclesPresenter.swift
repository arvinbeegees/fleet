//
//  VehiclesPresenter.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 10/09/2022.
//

import Foundation

class VehiclesPresenter {
    
    var vm: VehiclesVCVM!
    var vehiclesDataManager: VehiclesDataManager!
    
    init(vm: VehiclesVCVM,
         vehiclesDataManager: VehiclesDataManager) {
        self.vm = vm
        self.vehiclesDataManager = vehiclesDataManager
    }
    
    weak private var mVCDelegate : VehiclesVCDelegate?
    
    func setViewDelegate(_ vcDelegate: VehiclesVCDelegate?) {
        self.mVCDelegate = vcDelegate
    }
    
    func getVehicleList(apiKey: String) {
        vm.apiKey = apiKey
        
        let parameter = VehicleListParameter(key: apiKey)
        vehiclesDataManager.request(parameter: parameter) { [weak self] response in
            if let response = response {
                self?.vm.listing = self?.transform(response) ?? []
                self?.mVCDelegate?.onSuccessRetrieveVehicleList()
            }
        } failure: { [weak self] error in
            self?.mVCDelegate?.onError(error.errormessage)
        }
    }
    
    fileprivate func transform(_ response: VehicleListResponse) -> [VehiclesTVCellVM] {
        return response.response.map {
            return VehiclesTVCellVM(
                plate: $0.plate ?? "-",
                owner: $0.driverName ?? "-",
                location: $0.address ?? "-",
                speed: "\($0.speed ?? 0)",
                time: $0.timestamp ?? "-",
                objectId: $0.objectId
            )
        }
    }
}
