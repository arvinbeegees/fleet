//
//  VehiclesVC.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 09/09/2022.
//

import UIKit
import ProgressHUD

@objc protocol VehiclesVCDelegate {
    func onSuccessRetrieveVehicleList()
    func onError(_ errorMessage: String)
}

class VehiclesVC: BaseVC {
    
    // MARK: Properties - IBOutlet
    @IBOutlet weak var vehiclesTV: UITableView!
    
    var router: VehiclesRouter!
    var presenter: VehiclesPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.setViewDelegate(self)
        initUI()
        initTableView()
        initData()
    }
    
    func initUI() {
        title = "Vehicles"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .refresh,
            target: self,
            action: #selector(onTapRefresh)
        )
        navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        
        let rightBarButtonItem = UIButton(type: .system)
        rightBarButtonItem.setImage(UIImage.init(systemName: "key"), for: .normal)
        rightBarButtonItem.tintColor = UIColor.black
        rightBarButtonItem.sizeToFit()
        rightBarButtonItem.addTarget(
            self,
            action: #selector(onTapKey),
            for: .touchUpInside
        )
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBarButtonItem)
    }
    
    func initTableView() {
        vehiclesTV.tableFooterView = UIView()
        TVCellBuilder.shared.registerNib(.vehicles, for: vehiclesTV)
    }
    
    func initData() {
        showUpdateAPIKeyAlert()
    }
    
    func showUpdateAPIKeyAlert() {
        let alertController = UIAlertController(
            title: "Enter API Key:",
            message: nil,
            preferredStyle: .alert
        )
        alertController.addTextField()
        let cancelAction = UIAlertAction(
            title: "Cancel",
            style: .cancel
        )
        let okAction = UIAlertAction(
            title: "Ok",
            style: .default
        ) { [weak alertController] _ in
            if let textFields = alertController?.textFields,
               let answer = textFields.last,
               let text = answer.text {
                if text.count > 0 {
                    ProgressHUD.show()
                    self.presenter.getVehicleList(apiKey: text)
                }
            }
        }

        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        present(alertController, animated: true)
    }
    
    func showAlert(title: String) {
        let alertController = UIAlertController(
            title: title,
            message: nil,
            preferredStyle: .alert
        )
        let okAction = UIAlertAction(
            title: "Ok",
            style: .cancel
        )
        alertController.addAction(okAction)
        present(alertController, animated: true)
    }
    
    @objc func onTapRefresh() {
        ProgressHUD.show()
        presenter.getVehicleList(apiKey: presenter.vm.apiKey)
    }
    
    @objc func onTapKey() {
        showUpdateAPIKeyAlert()
    }
}

extension VehiclesVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.vm.listing.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return TVCellBuilder.shared.buildVehiclesTVCell(
            for: tableView,
            at: indexPath,
            vm: presenter.vm.listing[indexPath.row]
        )
    }
}

extension VehiclesVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router.navigateToLocationHistory(
            objectId: presenter.vm.listing[indexPath.row].objectId,
            apiKey: presenter.vm.apiKey
        )
    }
}

extension VehiclesVC: VehiclesVCDelegate {
    
    func onSuccessRetrieveVehicleList() {
        ProgressHUD.dismiss()
        vehiclesTV.reloadData()
    }
    
    func onError(_ errorMessage: String) {
        ProgressHUD.dismiss()
        showAlert(title: errorMessage)
    }
}

