//
//  VehiclesDataManager.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 09/09/2022.
//

import Foundation
import Alamofire

class VehiclesDataManager {
    
    func request(parameter: VehicleListParameter,
                 success: @escaping (VehicleListResponse?) -> (),
                 failure: @escaping (ErrorResponse) -> ()) {
        
        AF.request(
            BASE_URL + Endpoint.vehicles.rawValue,
            parameters: parameter.toDictionary()
        ).responseDecodable(of: VehicleListResponse.self) { response in
           switch response.result {
           case let .success(data):
             success(data)
           case .failure(_):
               do {
                   let errorResponse = try JSONDecoder().decode(
                       ErrorResponse.self,
                       from: response.data ?? Data()
                   )
                   failure(errorResponse)
               } catch {
                   failure(ErrorResponse(errormessage: error.localizedDescription))
               }
            }
        }
    }
}

