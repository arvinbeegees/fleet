//
//  TripHistoryDataManager.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 10/09/2022.
//

import Foundation
import Alamofire

class TripHistoryDataManager {
    
    func request(parameter: TripHistoryParameter,
                 success: @escaping (TripLocationListResponse?) -> (),
                 failure: @escaping (ErrorResponse) -> ()) {
        
        AF.request(
            BASE_URL + Endpoint.tripHistory.rawValue,
            parameters: parameter.toDictionary()
        ).responseDecodable(of: TripLocationListResponse.self) { response in
           switch response.result {
           case let .success(data):
             success(data)
           case .failure(_):
               do {
                   let errorResponse = try JSONDecoder().decode(
                       ErrorResponse.self,
                       from: response.data ?? Data()
                   )
                   failure(errorResponse)
               } catch {
                   failure(ErrorResponse(errormessage: error.localizedDescription))
               }
            }
        }
    }
}

