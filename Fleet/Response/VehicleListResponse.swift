//
//  VehicleListResponse.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 09/09/2022.
//

import Foundation

struct VehicleListResponse: Decodable {
    let response: [VehicleResponse]
    
    enum Key: String, CodingKey {
        case response
    }
}

struct VehicleResponse: Decodable {
    let timestamp: String?
    let speed: Int?
    let driverName: String?
    let address: String?
    let plate: String?
    let objectId: Int
    
    enum Key: String, CodingKey {
        case timestamp
        case speed
        case driverName
        case address
        case plate
        case objectId
    }
}

