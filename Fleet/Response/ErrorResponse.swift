//
//  ErrorResponse.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 10/09/2022.
//

import Foundation

struct ErrorResponse: Decodable {
    let errormessage: String
    
    enum Key: String, CodingKey {
        case errormessage
    }
}
