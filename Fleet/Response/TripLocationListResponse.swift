//
//  TripLocationListResponse.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 11/09/2022.
//

import Foundation

struct TripLocationListResponse: Codable {
    let response: [TripLocationResponse]
    
    enum Key: String, CodingKey {
        case response
    }
}

struct TripLocationResponse: Codable {
    let timestamp: String
    let Latitude: Double?
    let Longitude: Double?
    let Distance: Double?
    
    enum Key: String, CodingKey {
        case timestamp
        case Latitude
        case Longitude
        case Distance
    }
}
