//
//  TripHistoryParameter.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 10/09/2022.
//

import Foundation

struct TripHistoryParameter {
    
    let begTimestamp: String
    let endTimestamp: String
    let objectId: Int
    let key: String
    
    enum Key: String {
        case begTimestamp
        case endTimestamp
        case objectId
        case key
        case json
    }
    
    func toDictionary() -> [String : Any] {
        return [
            Key.begTimestamp.rawValue : begTimestamp,
            Key.endTimestamp.rawValue : endTimestamp,
            Key.objectId.rawValue : objectId,
            Key.key.rawValue : key,
            Key.json.rawValue: true
        ]
    }
}
