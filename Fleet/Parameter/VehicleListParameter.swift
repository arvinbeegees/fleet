//
//  VehicleListParameter.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 09/09/2022.
//

import Foundation

struct VehicleListParameter {
    
    let key: String
    
    enum Key: String {
        case key
        case json
    }
    
    func toDictionary() -> [String : Any] {
        return [
            Key.key.rawValue : key,
            Key.json.rawValue: true
        ]
    }
}
