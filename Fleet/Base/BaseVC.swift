//
//  BaseVC.swift
//  NYTDemo
//
//  Created by Arvin Sanmuga Rajah on 01/05/2019.
//  Copyright © 2019 Arvin Sanmuga Rajah. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {
    
    // MARK: Properties - Public
    var mActiveVC: UIViewController?
    var mIsLoading: Bool = false
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: Functions - Override
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: Navigation Stack
    @objc func popBackVC() {
        mActiveVC?.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissVC() {
        mActiveVC?.navigationController?.dismiss(
            animated: true,
            completion: nil
        )
    }
    
    // MARK: Scroll View
    @objc func dismissKeyboard() {}
    
    func scroll(at scrollView: UIScrollView,
                to view: UIView?) {
        guard view != nil else {
            return
        }
        
        let _view = view!
        
        DispatchQueue.main.async {
            let viewFrame = scrollView.convert(_view.frame,
                                               from: _view)
            let maxContentOffsetY = scrollView.contentSize.height - scrollView.bounds.size.height
            
            var toScrollOffsetY = viewFrame.origin.y
            if toScrollOffsetY > maxContentOffsetY {
                toScrollOffsetY = maxContentOffsetY
            }
            
            scrollView.setContentOffset(
                CGPoint.init(
                    x: scrollView.contentOffset.x,
                    y: toScrollOffsetY
                ),
                animated: true
            )
        }
    }
    
    /**
     Dismiss form by tapping content view, must initialize dismissKeyboard() function first.
     */
    public func addDismissKeyboardTapGesture(to view: UIView,
                                             dismiss from: UIScrollView,
                                             with sender: UIViewController) {
        let tapGesture = UITapGestureRecognizer.init(
            target: sender,
            action: #selector(dismissKeyboard)
        )
        view.addGestureRecognizer(tapGesture)
    }
}



