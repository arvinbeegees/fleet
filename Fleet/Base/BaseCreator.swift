//
//  BaseCreator.swift
//  NYTDemo
//
//  Created by Arvin Sanmuga Rajah on 01/05/2019.
//  Copyright © 2019 Arvin Sanmuga Rajah. All rights reserved.
//

import UIKit

class BaseCreator {
    
    class func getStoryboardName() -> String {
        fatalError("Must override getStoryboardName & getNavigationControllerIdentifier")
    }
    
    class func getNavigationControllerIdentifier() -> String {
        fatalError("Must override getStoryboardName & getNavigationControllerIdentifier")
    }
    
    static func initNavigationController() -> UINavigationController {
        let storyboard = UIStoryboard.init(
            name: getStoryboardName(),
            bundle: nil
        )
        
        return storyboard.instantiateViewController(
            withIdentifier: getNavigationControllerIdentifier()
        ) as! UINavigationController
    }
}
