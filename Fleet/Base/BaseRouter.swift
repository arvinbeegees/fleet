//
//  BaseRouter.swift
//  NYTDemo
//
//  Created by Arvin Sanmuga Rajah on 01/05/2019.
//  Copyright © 2019 Arvin Sanmuga Rajah. All rights reserved.
//

import UIKit

class BaseRouter: NSObject {
    
    func navigate(with identifier: String, via sender: UIViewController) {
        sender.performSegue(
            withIdentifier: identifier,
            sender: nil
        )
    }
    
    func push(to vc: UIViewController, animated: Bool = true, via sender: UIViewController) {
        sender.navigationController?.pushViewController(vc, animated: animated)
    }
    
    func present(_ nc: UINavigationController,
                 animated: Bool = true,
                 via sender: UIViewController,
                 completion:(()-> ())? = nil) {
        sender.navigationController?.present(
            nc,
            animated: animated,
            completion: completion
        )
    }
    
    func getSegueIdentifier<E: RawRepresentable>(_ vc: BaseVC?, identifier: E) -> String? {
        guard vc != nil else {
            return nil
        }
        return "\(vc!.className)\(identifier.rawValue)"
    }
}

extension NSObject {
    
    var className: String {
        return String(describing: type(of: self)).components(separatedBy: ".").last!
    }
    
    class var className: String {
        return String(describing: self).components(separatedBy: ".").last!
    }
}
