//
//  TVCellBuilder.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 09/09/2022.
//

import UIKit

class TVCellBuilder: NSObject {
    
    // MARK: Properties
    static let shared = TVCellBuilder()
    
    enum Identifier: String {
        case vehicles = "VehiclesTVCell"
    }
    
    // MARK: Register Cell
    func registerNib(_ identifier: Identifier,
                     for tableView: UITableView) {
        tableView.register(
            UINib(
                nibName: identifier.rawValue,
                bundle: nil
            ),
            forCellReuseIdentifier: identifier.rawValue
        )
    }
    
    // Vehicles
    func buildVehiclesTVCell(for tableView: UITableView,
                             at indexPath: IndexPath,
                             vm: VehiclesTVCellVM) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(
            withIdentifier: Identifier.vehicles.rawValue,
            for: indexPath) as? VehiclesTVCell
        if cell == nil {
            cell = VehiclesTVCell.init(
                style: .default,
                reuseIdentifier: Identifier.vehicles.rawValue
            )
        }
        cell?.fill(vm)
        return cell!
    }
}

