//
//  AppDelegate.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 09/09/2022.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        setupGoogleMaps()
        initNavigationBarAppearance()
        showVehiclesScene()
        return true
    }
    
    func setupGoogleMaps() {
        GMSServices.provideAPIKey("AIzaSyBTFL-3Xgg-NzBpBe9Q24ayhrUjtmgVBPM")
    }
    
    func initNavigationBarAppearance() {
        let appearance = UINavigationBarAppearance()
         appearance.configureWithOpaqueBackground()
         appearance.titleTextAttributes = [.foregroundColor: UIColor.black]
         let proxy = UINavigationBar.appearance()
         proxy.tintColor = .black
         proxy.standardAppearance = appearance
         proxy.scrollEdgeAppearance = appearance
    }
    
    func showVehiclesScene() {
        let vehiclesVC = VehiclesCreator.presentScene(VehiclesVCVM())
        changeRootViewController(vehiclesVC)
    }
    
    func changeRootViewController(_ rootVC: UIViewController) {
        if window == nil {
            window = UIWindow(frame: UIScreen.main.bounds)
        }
        
        window?.makeKeyAndVisible()
        window?.rootViewController = rootVC
    }
}

