//
//  VehiclesTVCellVM.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 09/09/2022.
//

import Foundation

struct VehiclesTVCellVM {
    
    let plate: String
    let owner: String
    let location: String
    let speed: String
    let time: String
    let objectId: Int
}
