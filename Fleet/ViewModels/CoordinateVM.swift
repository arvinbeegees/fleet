//
//  CoordinateVM.swift
//  Fleet
//
//  Created by Arvin Sanmuga Rajah on 11/09/2022.
//

import Foundation

struct CoordinateVM {
    let timestamp: String
    let latitude: Double
    let longitude: Double
    let distance: Double
}
